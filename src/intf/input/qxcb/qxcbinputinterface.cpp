#include <QtWidgets>
#include <xcb/xcb.h>
#include <xcb/xcb_keysyms.h>
#include <X11/keysym.h>
#include <QX11Info>
#include "qxcbinputinterface.h"
#include "burner.h"

static QXcbInputInterface *qxcbInput = nullptr;
static int xcbSymToFbaK(xcb_keysym_t code);
static char qKeyboardState[256] = { 0 };
static bool bKeyboardRead = false;

int QXcbInputSetCooperativeLevel(bool bExclusive, bool)
{
    qDebug() << __func__;
    return 0;
}

int QXcbInputExit()
{
    qDebug() << __func__;
    if (qxcbInput) {
        delete qxcbInput;
        qxcbInput = nullptr;
    }
    return 0;
}

int QXcbInputInit()
{
    qDebug() << __func__;

    qxcbInput = QXcbInputInterface::get();
    qxcbInput->install();
    memset(qKeyboardState, 0, 256);
    bKeyboardRead = false;
    return 0;
}

int QXcbInputStart()
{
    bKeyboardRead = false;
    return 0;
}

static int ReadJoystick()
{
    return 0;
}

int QXcbInputJoyAxis(int i, int nAxis)
{
    return 0;
}

static int ReadKeyboard()
{
    if (bKeyboardRead)
        return 0;
    qxcbInput->snapshot(qKeyboardState);
    bKeyboardRead = true;
    return 0;
}

static int ReadMouse()
{
    return 0;
}

int QXcbInputMouseAxis(int i, int nAxis)
{
    return 0;
}

static int JoystickState(int i, int nSubCode)
{
    return 0;
}

static int CheckMouseState(unsigned int nSubCode)
{
    return 0;
}

int QXcbInputState(int nCode)
{
    if (nCode < 0)
        return 0;

    if (nCode < 256) {
        if (!bKeyboardRead)
            ReadKeyboard();
        return qKeyboardState[nCode & 0xFF];
    }
    return 0;
}

int QXcbInputFind(bool CreateBaseline)
{
    return -1;
}

int QXcbInputGetControlName(int nCode, TCHAR* pszDeviceName, TCHAR* pszControlName)
{
    return 0;
}

struct InputInOut InputInOutQXcb = { QXcbInputInit, QXcbInputExit,
                                   QXcbInputSetCooperativeLevel, QXcbInputStart,
                                   QXcbInputState, QXcbInputJoyAxis, QXcbInputMouseAxis,
                                   QXcbInputFind, QXcbInputGetControlName, NULL,
                                   ("QtXCB") };


QXcbInputInterface *QXcbInputInterface::m_onlyInstance = nullptr;
QXcbInputInterface::QXcbInputInterface()
{
    memset(m_keys, 0, 256);
    m_isInitialized = false;
    m_xcbConnection = nullptr;
    m_xcbSymbols = nullptr;
}

QXcbInputInterface::~QXcbInputInterface()
{
    if (m_isInitialized)
        uninstall();
    m_onlyInstance = nullptr;
}

void QXcbInputInterface::install()
{
    if (m_isInitialized)
        return;
    m_xcbConnection = QX11Info::connection();
    m_xcbSymbols = xcb_key_symbols_alloc(m_xcbConnection);

    qApp->installNativeEventFilter(this);
    m_isInitialized = true;
}

void QXcbInputInterface::uninstall()
{
    if (!m_isInitialized)
        return;
    qApp->removeNativeEventFilter(this);
    xcb_key_symbols_free(m_xcbSymbols);
    m_xcbSymbols = nullptr;
    m_isInitialized = false;
}

QXcbInputInterface *QXcbInputInterface::get()
{
    if (m_onlyInstance)
        return m_onlyInstance;
    m_onlyInstance = new QXcbInputInterface();
    return m_onlyInstance;
}

bool QXcbInputInterface::nativeEventFilter(const QByteArray &eventType, void *message, long *)
{
    xcb_generic_event_t *event = static_cast<xcb_generic_event_t *>(message);
    xcb_key_press_event_t *key = static_cast<xcb_key_press_event_t *>(message);

    switch (event->response_type & ~0x80) {
    case XCB_KEY_PRESS: {
        xcb_keysym_t sym = xcb_key_symbols_get_keysym(m_xcbSymbols, key->detail, 0);
        m_keys[xcbSymToFbaK(sym)] = 1;
        break;
    }
    case XCB_KEY_RELEASE: {
        xcb_keysym_t sym = xcb_key_symbols_get_keysym(m_xcbSymbols, key->detail, 0);
        m_keys[xcbSymToFbaK(sym)] = 0;
        break;
    }
    }
    return false;
}

void QXcbInputInterface::snapshot(char *buffer, int keys)
{
    if (keys >= 256)
        keys = 256;
    memcpy(buffer, m_keys, keys);
}

int xcbSymToFbaK(xcb_keysym_t code)
{
    switch (code) {
    case XK_Up: return FBK_UPARROW;
    case XK_Down: return FBK_DOWNARROW;
    case XK_Left: return FBK_LEFTARROW;
    case XK_Right: return FBK_RIGHTARROW;

    case XK_a: return FBK_A;
    case XK_s: return FBK_S;
    case XK_d: return FBK_D;
    case XK_f: return FBK_F;
    case XK_g: return FBK_G;
    case XK_h: return FBK_H;
    case XK_j: return FBK_J;
    case XK_k: return FBK_K;
    case XK_l: return FBK_L;
    case XK_q: return FBK_Q;
    case XK_w: return FBK_W;
    case XK_e: return FBK_E;
    case XK_r: return FBK_R;
    case XK_t: return FBK_T;
    case XK_y: return FBK_Y;
    case XK_u: return FBK_U;
    case XK_i: return FBK_I;
    case XK_o: return FBK_O;
    case XK_p: return FBK_P;
    case XK_z: return FBK_Z;
    case XK_x: return FBK_X;
    case XK_c: return FBK_C;
    case XK_v: return FBK_V;
    case XK_b: return FBK_B;
    case XK_n: return FBK_N;
    case XK_m: return FBK_M;

    case XK_0: return FBK_M;
    case XK_1: return FBK_1;
    case XK_2: return FBK_2;
    case XK_3: return FBK_3;
    case XK_4: return FBK_4;
    case XK_5: return FBK_5;
    case XK_6: return FBK_6;
    case XK_7: return FBK_7;
    case XK_8: return FBK_8;
    case XK_9: return FBK_9;

    case XK_F1: return FBK_F1;
    case XK_F2: return FBK_F2;
    case XK_F3: return FBK_F3;
    case XK_F4: return FBK_F4;
    case XK_F5: return FBK_F5;
    case XK_F6: return FBK_F6;
    case XK_F7: return FBK_F7;
    case XK_F8: return FBK_F8;
    case XK_F9: return FBK_F9;
    case XK_F10: return FBK_F10;
    case XK_F11: return FBK_F11;
    case XK_F12: return FBK_F12;
    case XK_F13: return FBK_F13;
    case XK_F14: return FBK_F14;
    case XK_F15: return FBK_F15;

    case XK_Tab: return FBK_TAB;
    case XK_Shift_L: return FBK_LSHIFT;
    case XK_Control_L: return FBK_LCONTROL;
    case XK_Super_L: return FBK_LWIN;
    case XK_Alt_L: return FBK_LALT;

    case XK_Shift_R: return FBK_RSHIFT;
    case XK_Control_R: return FBK_RCONTROL;
    case XK_Super_R: return FBK_RWIN;
    case XK_Alt_R: return FBK_RALT;

    case XK_space: return FBK_SPACE;
    case XK_Escape: return FBK_ESCAPE;
    case XK_Return: return FBK_RETURN;
    case XK_BackSpace: return FBK_BACK;
    case XK_backslash: return FBK_BACKSLASH;
    case XK_slash: return FBK_SLASH;

    case XK_minus: return FBK_MINUS;
    case XK_equal: return FBK_EQUALS;
    case XK_bracketleft: return FBK_LBRACKET;
    case XK_bracketright: return FBK_RBRACKET;

    case XK_semicolon: return FBK_SEMICOLON;
    case XK_apostrophe: return FBK_APOSTROPHE;
    case XK_grave: return FBK_GRAVE;

    case XK_comma: return FBK_COMMA;
    case XK_period: return FBK_PERIOD;
    case XK_multiply: return FBK_MULTIPLY;
    case XK_Caps_Lock: return FBK_CAPITAL;

    case XK_Num_Lock: return FBK_NUMLOCK;
    case XK_Scroll_Lock: return FBK_SCROLL;

    case XK_Pause: return FBK_PAUSE;
    case XK_Home: return FBK_HOME;
    case XK_Page_Down: return FBK_NEXT;
    case XK_Page_Up: return FBK_PRIOR;
    case XK_Insert: return FBK_INSERT;
    case XK_Delete: return FBK_DELETE;

    default:
        return 0;
    }
}
